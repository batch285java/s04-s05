package com.zuitt.example;

public class Car {
    //Access modifier
    //These are used to restrict the scope of a class, constructor, variable, method or data

    //Four types of access Modifiers
    //1. Default - no keyword
    //2. Private - properties or method are only accessible within the class
    //3. Protected - properties and methods are only accessible by the class of the same package and the subclass present in any package
    //4. Public - properties and methods can be accessed anytime

    //Class creation
        //Four parts of class creation
    //1. Properties - are characteristics of objects


    private String name;
    private String brand;
    private int yearOfMake;

    //2. Constructor - used to create/instantiate an object

    //a. empty constructor - creates an object that doesn't have any arguments or parameters
    //instantiate an object from the car class

    public Car(){
        //b. Parameterized constructor - creates an object with arguments/parameters
        this.yearOfMake = 2000;

        //add driver
    }

    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;

    }

    //3 Getters and Setters - get and set the values of each property of the object

    public String getName(){
        return this.name;
    }

    public String getBrand(){
        return this.brand;
    }

    public int getYearOfMake(){
        return this.yearOfMake;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        if(yearOfMake <= 2023){
            this.yearOfMake = yearOfMake;
        }
    }

    public void drive(){
        System.out.println("The car is running. Vroom. Vroom");
    }


    private Driver driver;




}
