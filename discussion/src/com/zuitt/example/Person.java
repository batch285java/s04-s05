package com.zuitt.example;


public class Person implements Actions {
    public void sleep() {
        System.out.println("ZZZZZ snore....");
    }

    public void run(){
        System.out.println("Running");
    }


    //mini activity
    //Greetings interface.. morning greetings
    //implement it in person.java

    public void morninggreeting(){
        System.out.println("Good morning");
    }

    public void holidaygreet(){
        System.out.println("Happy holidays");
    }

}
