import com.zuitt.example.*;

public class Main {
    public static void main(String[] args){
        //OOP
        //Object-Oriented Programming
        //Programming model that allows developers to design a software around data or objects rather than function and logic


        //OOP Concepts
        //Object - Abstract idea that represents something in the real world
        //Ex. The concept of a dog
        //Class - representation of the object using code
        //Ex. Writing a code that would describe a dog/abstract.
        //Instance - unique copy of the idea, made "physical" or actual execution.
        //Ex. Instantiate a dog named Fluffy from the dog class

        //objects
         //States and Attributes - what is the idea about?
        //Properties of the specific object
        //Dog's name, breed, color, etc.,
        //Behaviours - what can the idea do?
        //Bark(), sit(), eat()


        //Four Pillars of OOP
            //1. Encapsulation - mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
            //"Data Hiding" - the variables of a class will be hidden from the other classes, and can be accessed only through the methods of their current class.

        Car myCar = new Car();
        myCar.drive();

        myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2025);

        System.out.println("My " + myCar.getName() + " " + myCar.getBrand() + "was made in " + myCar.getYearOfMake() + ".");


        //2. Inheritance
        //can be defined as the process where on class acquire the properties and methods of another class
        //with the use of inheritance, the information is made manageable in hierarchical order


        Dog myPet = new Dog();

        myPet.setName("Bantay");
        myPet.setColor("White");
        myPet.speak();
        myPet.setBreed("Askal");

        System.out.println("My dog is " + myPet.getName() + " and it is a " + myPet.getColor() + " " + myPet.getBreed());

        myPet.call();



        //3. Abstraction - is a process where all logic and complexity are hidden from the user

            //Interfaces
                //This is used to achieve total abstraction
                //creating abstract classes doesn't support "multiple inheritance" but it can be achieved with interface
            //interface allows multiple implementation, list functionalities

        //act as "contracts" wherein a class implements the interface should have methods that the interfaces has defined in the class

    Person child = new Person();

    child.sleep();
    child.run();
    child.holidaygreet();
    child.morninggreeting();


    StaticPoly myAddition = new StaticPoly();

    System.out.println(myAddition.addition(5,6));

        System.out.println(myAddition.addition(5,6, 7));

        System.out.println(myAddition.addition(5.5,6.6));

        //B. Dynamic or run-time polymorphism
            //function is overridden by replacing the definition of the method in the parent class in the child class

    }
}
