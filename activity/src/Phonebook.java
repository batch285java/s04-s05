import java.util.ArrayList;

public class Phonebook extends Contact{
    private ArrayList<Contact> contacts;

    public Phonebook() {
        this.contacts = new ArrayList<>();
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }
}


//output expected
//John Doe
//------------
//John doe has the following registered numbers:
//+63123456789
//+63987654212
//------------
//John Doe hsa the following registered addresses:
//my home in Quezon City
//my office in Makati City
//======================
//Jane Doe
//-----------
//Jane doe has the following registered numbers:
//+63657593424
//+63436534530
//-----------
//Jane Doe hsa the following registered addresses:
//my home in Caloocan City
//my office in Pasay City