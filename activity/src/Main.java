
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        
        Contact john = new Contact("John Doe", "+63123456789\n+63987654212", "my home in Quezon City\nmy office in Makati City");
        Contact jane = new Contact("Jane Doe", "+63657593424\n+63436534530", "my home in Caloocan City\nmy office in Pasay City");

       
        phonebook.addContact(john);
        phonebook.addContact(jane);

        
        for (Contact contact : phonebook.getContacts()) {
            System.out.println(contact.getName());
            System.out.println("------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getContactNumber());
            System.out.println("------------");
            System.out.println(contact.getName() + " has the following registered addresses:");
            System.out.println(contact.getAddress());
            System.out.println("======================");


        }
    }
}
