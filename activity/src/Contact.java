
    public class Contact {
        private String name;
        private String contactNumber;
        private String address;

        public Contact() {
            this.name = "Name";
            this.contactNumber = "Contact";
            this.address = "Address";
        }

        public Contact(String name, String contactNumber, String address) {
            this.name = name;
            this.contactNumber = contactNumber;
            this.address = address;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return address;
        }
    }


